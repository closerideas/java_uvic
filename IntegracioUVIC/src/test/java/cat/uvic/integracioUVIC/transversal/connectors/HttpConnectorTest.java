package cat.uvic.integracioUVIC.transversal.connectors;

import cat.uvic.integracioUVIC.transversal.connectors.HttpConnector;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Masu
 */
public class HttpConnectorTest
{
    private static List<Pair<String, String>> headers;
    private static List<Pair<String, String>> params;
    
    private enum Methods{GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE}
            
    public HttpConnectorTest(){}
    
    @BeforeClass
    public static void setUpClass()
    {
        (headers = new ArrayList<>()).add(new Pair("HeaderProva", "valorHeaderProva"));
        (params  = new ArrayList<>()).add(new Pair("ParamProva",  "valorParamProva"));
    }
    
    @AfterClass
    public static void tearDownClass(){}
    
    @Before
    public void setUp(){}
    
    @After
    public void tearDown(){}

    @Test
    public void testGetRestCallWithoutHeadersAndParams() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.GET.name(), "https://httpbin.org/get");   
        assertEquals ("200", result.getCodiResposta());
    }
    
    @Test
    public void testGetRestCallWithParamsWithoutHeaders() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.GET.name(), "https://httpbin.org/get", params);   
        assertTrue(result.getContingutResposta().getJsonObject("args").get("ParamProva") != null);
    }
    
    @Test
    public void testGetRestCallWithHeadersWithoutParams() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.GET.name(), "https://httpbin.org/get", null, null, headers);   
        assertTrue(result.getContingutResposta().getJsonObject("headers").get("Headerprova") != null);
    }
    
    @Test
    public void testGetRestCallWithHeadersAndParams() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.GET.name(), "https://httpbin.org/get", params, null, headers);   
        
        assertTrue(result.getContingutResposta().getJsonObject("headers").get("Headerprova") != null && 
                   result.getContingutResposta().getJsonObject("args").get("ParamProva")     != null);
    }
    
    @Test
    public void testGetRestCallErrorCode() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.GET.name(), "https://httpbin.org/get/prueba");   
        assertEquals ("404", result.getCodiResposta());
    }
    
    @Test
    public void testGetRestCallException() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        try
        {
            HttpConnector.RestCall(Methods.GET.name(), "https://asfdasfasdfsaf.org/get/prueba");
        }
        catch(UnknownHostException ue)
        {
            System.out.println("S'ha generat la excepció esperada");
        }
        
        assertTrue(true);
    }
    
    @Test
    public void testPostRestCallWithoutHeadersAndParams() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.POST.name(), "https://httpbin.org/post");   
        assertEquals ("200", result.getCodiResposta());
    }
    
    @Test
    public void testPostRestCallWithParamsWithoutHeaders() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.POST.name(), "https://httpbin.org/post", params);   
        assertTrue(result.getContingutResposta().getJsonObject("args").get("ParamProva") != null);
    }
    
    @Test
    public void testPostRestCallWithHeadersWithoutParams() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.POST.name(), "https://httpbin.org/post", null, null, headers);   
        assertTrue(result.getContingutResposta().getJsonObject("headers").get("Headerprova") != null);
    }
    
    @Test
    public void testPostRestCallWithHeadersAndParams() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.POST.name(), "https://httpbin.org/post", params, null, headers);
        
        assertTrue(result.getContingutResposta().getJsonObject("headers").get("Headerprova") != null && 
                   result.getContingutResposta().getJsonObject("args").get("ParamProva")     != null);
    }
    
    @Test
    public void testPostRestCallErrorCode() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse result = HttpConnector.RestCall(Methods.POST.name(), "https://httpbin.org/post/prueba");   
        assertEquals ("404", result.getCodiResposta());
    }
    
    @Test
    public void testPostRestCallException() throws MalformedURLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        try
        {
            HttpConnector.RestCall(Methods.GET.name(), "https://asfdasfasdfsaf.org/post/prueba");
        }
        catch(UnknownHostException ue)
        {
            System.out.println("S'ha generat la excepció esperada");
        }
        
        assertTrue(true);
    }
}
