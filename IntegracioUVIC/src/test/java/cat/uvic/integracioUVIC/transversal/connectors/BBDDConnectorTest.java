package cat.uvic.integracioUVIC.transversal.connectors;

import cat.uvic.integracioUVIC.transversal.connectors.BBDDConnector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Masu
 */
public class BBDDConnectorTest {
    
    public BBDDConnectorTest() {}
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testConectarBBDD_MySQL() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        Connection conexionMySQL = BBDDConnector.getConnection("jdbc:mysql://localhost:3306/POC_UVIC?serverTimezone=UTC", "masu", "CLOSER2017");
        assertTrue(conexionMySQL != null);
    }
    
    @Test
    public void testExecuteQuery_MySQL() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        String nomGrup = null;
        
        Connection conexionMySQL = BBDDConnector.getConnection("jdbc:mysql://localhost:3306/POC_UVIC?serverTimezone=UTC", "masu", "CLOSER2017");
        
        PreparedStatement statement = conexionMySQL.prepareStatement("SELECT * FROM POC_UVIC.GRUPS WHERE ID_GRUP = ?");
        statement.setInt(1, 2);
        
        ResultSet resultSet = statement.executeQuery();
        
        if(resultSet.next())
        {
            nomGrup = resultSet.getNString("NOM_GRUP");
        }
        
        assertNotNull(nomGrup);
    }
    
    @Test
    public void testConectarBBDD_MySQL_With_ClosedConnection() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {  
        String nomGrup = null;
        
        Connection conexionMySQL = BBDDConnector.getConnection("jdbc:mysql://localhost:3306/POC_UVIC?serverTimezone=UTC", "masu", "CLOSER2017");
        conexionMySQL.close();
        
        conexionMySQL = BBDDConnector.getConnection("jdbc:mysql://localhost:3306/POC_UVIC?serverTimezone=UTC", "masu", "CLOSER2017");
        
        PreparedStatement statement = conexionMySQL.prepareStatement("SELECT * FROM POC_UVIC.GRUPS WHERE ID_GRUP = ?");
        statement.setInt(1, 2);
        
        ResultSet resultSet = statement.executeQuery();
        
        if(resultSet.next())
        {
            nomGrup = resultSet.getNString("NOM_GRUP");
        }
        
        assertNotNull(nomGrup);
    }
    
    @Test
    public void testExcuteQuery_MySQLConnectionClosed() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {          
        Connection conexionMySQL = BBDDConnector.getConnection("jdbc:mysql://localhost:3306/POC_UVIC?serverTimezone=UTC", "masu", "CLOSER2017");
        conexionMySQL.close();
        
        try
        {
            PreparedStatement statement = conexionMySQL.prepareStatement("SELECT * FROM POC_UVIC.GRUPS WHERE ID_GRUP = ?");
            statement.setInt(1, 1);
        
            statement.executeQuery();
        }
        catch(SQLNonTransientConnectionException sqlntce)
        {
            assertTrue(true);
        }
    }
    
}
