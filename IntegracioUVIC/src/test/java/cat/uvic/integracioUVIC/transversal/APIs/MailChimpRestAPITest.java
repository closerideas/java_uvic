/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.uvic.integracioUVIC.transversal.APIs;

import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import javax.json.Json;
import javax.json.JsonObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Masu
 */
public class MailChimpRestAPITest
{    
    private static String idList;
    private static String apiKeyMailchimpCloser;
    private static String urlMailchimpListsCloser;    
    
    public MailChimpRestAPITest() {}
    
    @BeforeClass
    public static void setUpClass()
    {
        idList                  = "df2f4c30fc";
        apiKeyMailchimpCloser   = "apikey cc6267a73e4402b55a4b180c23ccd2e4-us3";
        urlMailchimpListsCloser = "https://us3.api.mailchimp.com/3.0/";
    }   
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown() {}

    @Test
    public void testMailChimp_GetNumMembersList_NumMembers() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("fields", "total_items"));
        
        RestResponse resposta = MailChimpRestAPI.getInfoList(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, parametres);
        assertNotNull(resposta.getContingutResposta().getInt("total_items"));
    }
    
    @Test
    public void testMailChimp_GetMembersList_Susbscribed() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("fields", "total_items,members.email_address"));
        parametres.add(new Pair("status", "subscribed"));
        parametres.add(new Pair("count", "50"));
        
        RestResponse resposta = MailChimpRestAPI.getInfoList(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, parametres);
        assertNotNull(resposta.getContingutResposta().getInt("total_items"));
    }
    
    @Test
    public void testMailChimp_GetMembersList_SubscribedAndCleaned() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("fields", "total_items"));
        parametres.add(new Pair("status", "unsubscribed,cleaned"));
        
        RestResponse resposta = MailChimpRestAPI.getInfoList(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, parametres);
        assertNotNull(resposta.getContingutResposta().getInt("total_items"));
    }
    
    @Test
    public void testMailChimp_GetMembersList_Mail() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("fields", "members.email_address"));
        parametres.add(new Pair("status", "subscribed"));
        
        RestResponse resposta = MailChimpRestAPI.getInfoList(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, parametres);
        assertTrue(resposta.getContingutResposta().getJsonArray("members").getJsonObject(0).getString("email_address").contains("@"));
    }
    
    @Test
    public void testMailChimp_ListNotFound() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {        
        RestResponse resposta = MailChimpRestAPI.getInfoList(urlMailchimpListsCloser, apiKeyMailchimpCloser, "inventado", null);
        assertTrue(resposta.getContingutResposta().getString("RequestError").contains("404"));
    }
    
    @Test
    public void testMailChimp_addContact() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {        
        double randomValue = Math.random();
        JsonObject contingut = Json.createReader(new StringReader("{"
                                                                  + "\"email_address\":\"loremipsum" + randomValue + "@loremipsum.com\","
                                                                  + "\"status\":\"subscribed\","
                                                                  + "\"merge_fields\": {"
                                                                                    + " \"NAME\": \"Proba Integració\""
                                                                                    + "}"
                                                                + "}")).readObject();
        
        RestResponse resposta = MailChimpRestAPI.addNewContactToList(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, null, contingut);
        
        assertEquals("subscribed", resposta.getContingutResposta().getString("status"));
    }
    
    @Test
    public void testMailChimp_Unsubscribe() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {        
        JsonObject contingut = Json.createReader(new StringReader("{"
                                                                  + "\"email_address\":\"loremipsum444@loremipsum.com\","
                                                                  + "\"status\":\"unsubscribed\""
                                                                + "}")).readObject();
        
        RestResponse resposta = MailChimpRestAPI.modifyContactoFromList(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, null, contingut);

        assertEquals("unsubscribed", resposta.getContingutResposta().getString("status"));
    }
    
    @Test
    public void testMailChimp_BatchActions() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {                
        JsonObject contingut = Json.createReader(new StringReader("{\n" +
                                                                    "  \"operations\": [\n" +
                                                                    "    {\n" +
                                                                    "      \"method\": \"POST\",\n" +
                                                                    "      \"path\": \"lists/df2f4c30fc/members\",\n" +
                                                                    "      \"body\": \"{\\\"email_address\\\":\\\"loremipsumPOST2" + Math.random() + "@loremipsum.com\\\", \\\"status\\\":\\\"subscribed\\\", \\\"merge_fields\\\": {\\\"NAME\\\": \\\"Proba Integració\\\"}}\"\n" +
                                                                    "    },\n" +
                                                                    "    {\n" +
                                                                    "      \"method\": \"PUT\",\n" +
                                                                    "      \"path\": \"lists/df2f4c30fc/members/E3322A4CF0CEB9D266F1CD87B3B21997\",\n" +
                                                                    "      \"body\": \"{\\\"email_address\\\":\\\"loremipsum444@loremipsum.com\\\", \\\"status\\\":\\\"subscribed\\\"}\"\n" +
                                                                    "    },\n" +
                                                                    "    {\n" +
                                                                    "      \"method\": \"PUT\",\n" +
                                                                    "      \"path\": \"lists/df2f4c30fc/members/E3322A4CF0CEB9D266F1CD87B3B21997\",\n" +
                                                                    "      \"body\": \"{\\\"email_address\\\":\\\"loremipsum444@loremipsum.com\\\", \\\"merge_fields\\\": {\\\"NAME\\\": \\\"Proba Update\\\"}}\"\n" +
                                                                    "    }\n" +
                                                                    "  ]\n" +
                                                                    "}")).readObject();
        

        RestResponse resposta = MailChimpRestAPI.executeBatch(urlMailchimpListsCloser, apiKeyMailchimpCloser, null, contingut);

        assertEquals("pending", resposta.getContingutResposta().getString("status"));
    }
    
    @Test
    public void testMailChimp_getListFields() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {        
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("fields", "merge_fields.tag"));
        
        RestResponse resposta = MailChimpRestAPI.getListFields(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, parametres, null);

        assertNotNull(resposta.getContingutResposta().getJsonArray("merge_fields").getJsonObject(0).getString("tag"));
    }
    
    @Test
    public void testMailChimp_updateListFields() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {        
        JsonObject contingut = Json.createReader(new StringReader("{\n" +
                                                                    "	\"tag\":\"" + ("LNAME" + Math.random()).substring(9) + "\",\n" +
                                                                    "	\"name\":\"Apellido" + Math.random() + "\",\n" +
                                                                    "	\"type\":\"text\"\n" +
                                                                    "}")).readObject();
        
        RestResponse resposta = MailChimpRestAPI.updateListFields(urlMailchimpListsCloser, apiKeyMailchimpCloser, idList, null, contingut);

        assertEquals("200", resposta.getCodiResposta());
    }
}
