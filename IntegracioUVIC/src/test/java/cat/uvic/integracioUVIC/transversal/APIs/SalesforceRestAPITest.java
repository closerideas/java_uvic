package cat.uvic.integracioUVIC.transversal.APIs;

import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author Masu
 */
public class SalesforceRestAPITest
{        
    private static String urlSalesforceLoginUVIC;
    private static String urlSalesforceReportsUVIC;
    
    private static String username;
    private static String password;
    private static String client_id;
    private static String client_secret;
    private static String grant_type;
    
    private static String reportId;
        
    public SalesforceRestAPITest() {}
    
    @BeforeClass
    public static void setUpClass() 
    {
        urlSalesforceLoginUVIC = "https://cs83.salesforce.com/services/oauth2/token";
        urlSalesforceReportsUVIC = "https://cs83.salesforce.com/services/data/v42.0/analytics/reports/";
        
        username  = "joan.busquiel@uvic.cat.desarrollo";
        password  = "st.Praxedis16";
        client_id = "3MVG9w8uXui2aB_o.HRULit.JpHrpt0LkfQF.9TQH..214dZF0EL4SrIa1OoZKE24it03z15FPb4UCtGDNCF5";
        client_secret = "7185473240455495271";
        grant_type = "password";
        
        reportId = "00O4E000000WpKp";
    }
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown() {}

    
    
    @Test
    public void testSalesForce_Login() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse resposta = SalesforceRestAPI.doLogin(urlSalesforceLoginUVIC, username, password, client_id, client_secret, grant_type);
        
        assertEquals ("200", resposta.getCodiResposta());
    }
    
    @Test
    public void testSalesForce_Login_GetToken() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse resposta = SalesforceRestAPI.doLogin(urlSalesforceLoginUVIC, username, password, client_id, client_secret, grant_type);
        
        assertNotNull(resposta.getContingutResposta().get("access_token"));
    }
    
    @Test
    public void testSalesForce_GetReport_call() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        SalesforceRestAPI.doLogin(urlSalesforceLoginUVIC, username, password, client_id, client_secret, grant_type);
        RestResponse resposta = SalesforceRestAPI.getReport(urlSalesforceReportsUVIC, reportId);
        
        assertEquals("200", resposta.getCodiResposta());
    }
    
    @Test
    public void testSalesForce_GetReport_info() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        SalesforceRestAPI.doLogin(urlSalesforceLoginUVIC, username, password, client_id, client_secret, grant_type);
        RestResponse resposta = SalesforceRestAPI.getReport(urlSalesforceReportsUVIC, reportId);
        
        assertEquals("PruebaIntegracion", resposta.getContingutResposta().getJsonObject("attributes").getString("reportName"));
    }
}
