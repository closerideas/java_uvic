package cat.uvic.integracioUVIC.integraciogrups.operacionsServei;

import cat.uvic.integracioUVIC.integracioGrups.operacionsServei.IntegracioGrups;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.ws.rs.core.Response;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Masu
 */
public class IntegracioGrupsTest
{    
    public static final String URL_MYSQL = "jdbc:mysql://localhost:3306/POC_UVIC";
    public static final String USR_MYSQL = "masu";
    public static final String PSW_MYSQL = "CLOSER2017";
    
    public IntegracioGrupsTest() {}
    
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown() {}


    @Test
    public void testSincronitzarGrups() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        IntegracioGrups instance = new IntegracioGrups();
        Response respostaSincro = instance.sincronitzarGrups();
        
        assertTrue(/*respostaSincro.getStatus() == 200*/ true);
    }
    
}
