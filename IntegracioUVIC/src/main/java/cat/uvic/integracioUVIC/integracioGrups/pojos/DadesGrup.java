package cat.uvic.integracioUVIC.integracioGrups.pojos;

import java.util.Map;
import javax.json.JsonObject;

/**
 *
 * @author Masu
 */
public class DadesGrup
{
    private int idPlataformaGrup;
    private Map<String,Object> configuracioGrup;
    private JsonObject dadesConfiguracioGrup;

    public DadesGrup(int idPlataformaGrup, Map<String, Object> configuracioGrup, JsonObject dadesConfiguracioGrup)
    {
        this.idPlataformaGrup      = idPlataformaGrup;
        this.configuracioGrup      = configuracioGrup;
        this.dadesConfiguracioGrup = dadesConfiguracioGrup;
    }
    
    public int getIdPlataformaGrup() {
        return idPlataformaGrup;
    }

    public void setIdPlataformaGrup(int idPlataformaGrup) {
        this.idPlataformaGrup = idPlataformaGrup;
    }

    public Map<String,Object> getConfiguracioGrup() {
        return configuracioGrup;
    }

    public void setConfiguracioGrup(Map<String,Object> configuracioGrup) {
        this.configuracioGrup = configuracioGrup;
    }

    public JsonObject getDadesConfiguracioGrup() {
        return dadesConfiguracioGrup;
    }

    public void setDadesConfiguracioGrup(JsonObject dadesConfiguracioGrup) {
        this.dadesConfiguracioGrup = dadesConfiguracioGrup;
    }    
}
