package cat.uvic.integracioUVIC.integracioGrups.daos;

import cat.uvic.integracioUVIC.transversal.APIs.SalesforceRestAPI;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import cat.uvic.integracioUVIC.transversal.utils.SalesForceUtils;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class SalesforceDAO
{
    private static final Pattern EMAIL_REGEX =  Pattern.compile("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    
    private String tokenType;
    private String accessToken;
    private String username;
    private String password;
    private String client_id;
    private String client_secret;
    private String grant_type;
    private String urlSalesforceLogin;
    private String urlSalesforceReports;
    
    private static final Logger LOGGER = LogManager.getLogger(SalesforceDAO.class);
        
    //Constructor sense grant_type
    public SalesforceDAO(String urlSalesforceLogin, String username, String password, String client_id, String client_secret)
    {
        this(urlSalesforceLogin, username, password, client_id, client_secret, "password");
    }
    
    //Constructor amb tots els elements
    public SalesforceDAO(String urlSalesforceLogin, String username, String password, String client_id, String client_secret, String grant_type)
    {
        this.username = username;
        this.password = password;
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.urlSalesforceLogin = urlSalesforceLogin;
        this.grant_type = grant_type;
    }
    
    private RestResponse loginSalesforce() throws IOException, URISyntaxException, NoSuchAlgorithmException
    {       
        LOGGER.debug("Fem login a Salesforce");
        if(tokenType != null && accessToken != null && SalesforceRestAPI.isLoged(tokenType, accessToken))
        {
            RestResponse resposta = new RestResponse();
            resposta.setCodiResposta("200");
            return resposta;
        }
        
        return SalesforceRestAPI.doLogin(urlSalesforceLogin, username, password, client_id, client_secret, grant_type);
    }
    
    public RestResponse getReportMetadata(String urlSalesforceReports, String reportId) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        return SalesforceRestAPI.getReportMetadata(urlSalesforceReports, reportId);
    }
        
    public RestResponse getInfoContactes(String reportId) throws IOException, URISyntaxException, NoSuchAlgorithmException 
    {
        return getInfoContactes(urlSalesforceReports, reportId, null, null);
    }
    
    public RestResponse getInfoContactes(String urlSalesforceReports, String reportId, String campMail, String campMultivalor) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {       
        //Guardem la URL dels reports per si s'ha d'utilitzar en un futur
        this.urlSalesforceReports = urlSalesforceReports;
        
        //Fem el login
        RestResponse respostaLogin = loginSalesforce();
        
        //Si falla el login retornem l'error
        if(!respostaLogin.validateResponse()) return respostaLogin;
        
        //Obtenim les dades del report
        LOGGER.debug("Obtenim les dades del report: " + reportId);
        
        boolean concatenar = true;
        String mailInicial = "";
        
        JsonArray contactesReport;
        JsonArrayBuilder arrayContactes = Json.createArrayBuilder();
        
        RestResponse reportContactes = null;    
        RestResponse metadataActual = getReportMetadata(urlSalesforceReports, reportId);  
        
        String aplicacioFiltre = "";
        
        /* Si tenemos una condición de filtro la obtenemos*/
        if(!metadataActual.getContingutResposta().getJsonObject("reportMetadata").isNull("reportBooleanFilter"))
        {
            aplicacioFiltre = metadataActual.getContingutResposta().getJsonObject("reportMetadata").getJsonString("reportBooleanFilter").getString();
        }
        
        JsonArray condicionsFiltre = metadataActual.getContingutResposta().getJsonObject("reportMetadata").getJsonArray("reportFilters");
        

        while(concatenar)
        {
            reportContactes = SalesforceRestAPI.getReportFiltered(urlSalesforceReports, reportId, SalesForceUtils.getFiltreReport(aplicacioFiltre, condicionsFiltre, mailInicial, campMail, "greaterThan", campMail, "Asc"));

            contactesReport = reportContactes.getContingutResposta().getJsonObject("factMap").getJsonObject("T!T").getJsonArray("rows");

            
            //Si l'array està buit, obtenim l'últim mail
            if(!contactesReport.isEmpty())
            {
                int ultimContacte = contactesReport.size()-1;
                Optional<JsonValue> mail = null;

                while(mail == null && ultimContacte >= 0)
                {
                    mail = contactesReport.getJsonObject(ultimContacte).getJsonArray("dataCells").parallelStream().filter(t -> !((JsonObject)t).isNull("value")).filter(t -> EMAIL_REGEX.matcher(((JsonObject)t).getString("value")).matches()).findFirst();

                    if(mail.isPresent()) mailInicial = ((JsonObject)mail.get()).getString("value");
                    ultimContacte--;
                }
            }
            else
            {
                concatenar = false;
            }
            
            contactesReport.stream().forEach(t -> arrayContactes.add(t));
        }
        
        //Modifiquem l'estructura del JSON a una que ens serveixi per la integració (Array fila - columna - valor)
        SalesForceUtils.parsejarContactes(reportContactes, arrayContactes.build(), campMail, campMultivalor);
        
        //Retornem la informació del report.
        return reportContactes;
    }

/////////////////////////////////////////////////////             GETTERS I SETTERS                //////////////////////////////////////////////  
    
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getClient_id()
    {
        return client_id;
    }

    public void setClient_id(String client_id)
    {
        this.client_id = client_id;
    }

    public String getClient_secret()
    {
        return client_secret;
    }

    public void setClient_secret(String client_secret)
    {
        this.client_secret = client_secret;
    }

    public String getGrant_type()
    {
        return grant_type;
    }

    public void setGrant_type(String grant_type)
    {
        this.grant_type = grant_type;
    }
    
    public String getUrlSalesforceLogin()
    {
        return urlSalesforceLogin;
    }

    public void setUrlSalesforceLogin(String urlSalesforceLogin)
    {
        this.urlSalesforceLogin = urlSalesforceLogin;
    }

    public String getUrlSalesforceReports()
    {
        return urlSalesforceReports;
    }

    public void setUrlSalesforceReports(String urlSalesforceReports)
    {
        this.urlSalesforceReports = urlSalesforceReports;
    }
}
