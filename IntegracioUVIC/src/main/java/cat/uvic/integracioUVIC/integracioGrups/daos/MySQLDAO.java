package cat.uvic.integracioUVIC.integracioGrups.daos;

import cat.uvic.integracioUVIC.transversal.APIs.MySQLBBDDAPI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Masu
 */
public class MySQLDAO
{
    private String url;
    private String usuari;
    private String password;
    private String driver;
    private String zonaHoraria;
    
    private Connection connexio;
    
    private static final Logger LOGGER = LogManager.getLogger(MySQLDAO.class);
    
    public MySQLDAO(String url, String usuari, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        this.driver = null;
        this.zonaHoraria = null;
        this.url = url;
        this.usuari = usuari;
        this.password = password;
        this.connexio = MySQLBBDDAPI.getConnectionMySQL(url, usuari, password, driver, zonaHoraria);
    }
    
    public MySQLDAO(String url, String usuari, String password, String driver) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        this.driver = driver;
        this.zonaHoraria = null;
        this.url = url;
        this.usuari = usuari;
        this.password = password;
        this.connexio = MySQLBBDDAPI.getConnectionMySQL(url, usuari, password, driver, zonaHoraria);
    }
    
    public MySQLDAO(String url, String usuari, String password, String driver, String zonaHoraria) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        this.driver = driver;
        this.zonaHoraria = zonaHoraria;
        this.url = url;
        this.usuari = usuari;
        this.password = password;
        this.connexio = MySQLBBDDAPI.getConnectionMySQL(url, usuari, password, driver, zonaHoraria);
    }
    
    public void closeConnection()
    {
        try{if(connexio != null && !connexio.isClosed()) connexio.close();}
        catch(SQLException sqle){}
    }
    
    public Connection getConexioBBDD() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        //Singleton
        if(connexio == null || !connexio.isValid(0)) connexio = MySQLBBDDAPI.getConnectionMySQL(url, usuari, password, driver, zonaHoraria);
        
        return connexio;
    }
 
    public ResultSet doQuery(String query) throws SQLException
    {
        return doQuery(query, null);
    }
    
    public ResultSet doQuery(String query, List<Object> parametres) throws SQLException
    {
        PreparedStatement statement = null;
                
        try
        {
            statement = configureStatement(query, parametres);
            
            LOGGER.debug("Crida BBDD: " + statement);
            return statement.executeQuery();
        }
        finally
        {
            try{ if(statement != null) statement.closeOnCompletion();} catch(SQLException e){}
        }
    }
    
    public boolean doDML(String query, List<Object> parametres) throws SQLException
    {
        PreparedStatement statement = null;
        
        try
        {
            statement = configureStatement(query, parametres);
            LOGGER.debug("Crida BBDD: " + statement);
            return statement.execute();
        }
        finally
        {
            try{ if(statement != null) statement.closeOnCompletion();} catch(SQLException e){}
        }
    }
    
    private PreparedStatement configureStatement(String query, List<Object> parametres) throws SQLException
    {
        PreparedStatement statement = connexio.prepareStatement(query);
        
        if(parametres != null)
        {
            for(int index = 0; index < parametres.size(); index++)
            {
                switch(parametres.get(index) == null ? "null" : parametres.get(index).getClass().getSimpleName())
                {
                    case "null":
                        statement.setNull(index + 1, Types.NULL);
                        break;
                    case "String":
                        statement.setString(index + 1, (String)parametres.get(index));
                        break;
                    case "Integer":
                        statement.setInt(index + 1, (Integer)parametres.get(index));
                        break;
                    default:
                        statement.setObject(index + 1, parametres.get(index));    
                }    
            }
        }
        statement.toString();
        return statement;
    }
    
    
    /////////////////////////////////////////////////////             GETTERS I SETTERS                //////////////////////////////////////////////      
    
    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUsuari()
    {
        return usuari;
    }

    public void setUsuari(String usuari)
    {
        this.usuari = usuari;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getDriver()
    {
        return driver;
    }

    public void setDriver(String driver)
    {
        this.driver = driver;
    }

    public String getZonaHoraria() {
        return zonaHoraria;
    }

    public void setZonaHoraria(String zonaHoraria)
    {
        this.zonaHoraria = zonaHoraria;
    }         
}
