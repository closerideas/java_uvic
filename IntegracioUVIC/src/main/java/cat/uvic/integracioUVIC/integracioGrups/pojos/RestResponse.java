package cat.uvic.integracioUVIC.integracioGrups.pojos;

import javax.json.JsonObject;

/**
 *
 * @author Masu
 */
public class RestResponse
{
    private String codiResposta;
    private String missatgeResposta;
    private JsonObject contingutResposta;
    
    public boolean validateResponse()
    {
        if(codiResposta == null) return false;
        
        int codi = Integer.parseInt(codiResposta);
        return 200 <= codi && codi < 300;
    }

    public String getCodiResposta()
    {
        return codiResposta;
    }

    public void setCodiResposta(String codiResposta)
    {
        this.codiResposta = codiResposta;
    }

    public String getMissatgeResposta()
    {
        return missatgeResposta;
    }

    public void setMissatgeResposta(String missatgeResposta)
    {
        this.missatgeResposta = missatgeResposta;
    }

    public JsonObject getContingutResposta()
    {
        return contingutResposta;
    }

    public void setContingutResposta(JsonObject contingutResposta)
    {
        this.contingutResposta = contingutResposta;
    }
}
