package cat.uvic.integracioUVIC.integracioGrups.operacionsServei;

import cat.uvic.integracioUVIC.integracioGrups.daos.MailChimpDAO;
import cat.uvic.integracioUVIC.integracioGrups.daos.MySQLDAO;
import cat.uvic.integracioUVIC.integracioGrups.daos.SalesforceDAO;
import cat.uvic.integracioUVIC.integracioGrups.integracionsSistemes.Salesforce_Mailchimp;
import cat.uvic.integracioUVIC.integracioGrups.recursos.MySQLQuerys;
import cat.uvic.integracioUVIC.integracioGrups.pojos.DadesGrup;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import cat.uvic.integracioUVIC.transversal.recursos.GenericMySQLQuerys;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Masu
 */
@Path("/IntegracioGrups")
public class IntegracioGrups
{
    private MySQLDAO mySQLDAO = null;
    private MailChimpDAO mailChimpDAO = null;
    private SalesforceDAO salesForceDAO = null;
    
    private static final Logger LOGGER = LogManager.getLogger(IntegracioGrups.class.getName());

    @GET
    @Produces("text/html; charset=UTF-8")
    public Response sincronitzarGrups() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, URISyntaxException, NoSuchAlgorithmException
    {     
        //Comprovem si tenim inicialitzades les dades i si no, les obtenim
        inicialitzar();

        LOGGER.info("Iniciem el procés d'Integració");
        
        int idHistoricDesti = 0;
        int idHistoricGrup = 0;
        boolean tempsSuficient = true;
        Response resultatProces = null;
       
        try
        {
            //Obrim la conexio amb la BBDD
            mySQLDAO = new MySQLDAO(System.getProperty("urlMySQL"), System.getProperty("usuariMySQL"), System.getProperty("passwordMySQL"));

            //Obtenim el llistat de grups a sincronitzar
            try(ResultSet llistaGrupsRS = mySQLDAO.doQuery(MySQLQuerys.INTEGRACIO_GET_GRUPS))
            {
                //Tractem cada grup de la llista
                while(llistaGrupsRS.next())
                {    
                    //Comprovem si ha passat prou temps per tornar a processar el grup
                    try(ResultSet ultimHistoricRS = mySQLDAO.doQuery(MySQLQuerys.INTEGRACIO_HISTORIC_ULTIMA_SINCRO, of(llistaGrupsRS.getObject("ID_GRUP")).collect(toList())))
                    {
                        if(ultimHistoricRS.next() && ultimHistoricRS.getTimestamp("ULTIMA_SINCRO") != null)
                        {                             
                            //Comprovem si ha passat prou temps
                            tempsSuficient = ultimHistoricRS.getTimestamp("ULTIMA_SINCRO").toInstant().isBefore(LocalDateTime.now().toInstant(ZoneOffset.UTC).minus(Duration.ofMinutes(llistaGrupsRS.getLong("FREQUENCIA_SINCRO"))));
                        }
                        
                        if("N".equalsIgnoreCase(llistaGrupsRS.getString("ACTIU")) || "PROCESSANT".equals(ultimHistoricRS.getString("ESTAT_INTEGRACIO")) || !tempsSuficient)
                        {
                            if(!tempsSuficient)                                                         LOGGER.info("No ha passat prou temps des de la última sincronització del grup " + llistaGrupsRS.getObject("NOM_GRUP") + " de " + ultimHistoricRS.getTimestamp("ULTIMA_SINCRO") + ". Passem a processar el següent grup");
                            else if("PROCESSANT".equals(ultimHistoricRS.getString("ESTAT_INTEGRACIO"))) LOGGER.warn("La última sincronització del grup " + llistaGrupsRS.getObject("NOM_GRUP") + " encara s'està PROCESSANT. Passem a processar el següent grup");
                            
                            continue;
                        }
                    }

                    LOGGER.info("Processem el grup " + llistaGrupsRS.getObject("NOM_GRUP") + ": Plataforma orgien " + llistaGrupsRS.getInt("PLATAFORMA_ORIGEN") + " - Configuració " + llistaGrupsRS.getInt("ID_CONFIG_ORIGEN"));

                    try
                    {
                        String resultatGrup = "FINALITZAT";

                        //Registrem l'històric del grup i guardem l'ID generat
                        mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_GRUPS_INICI, of(llistaGrupsRS.getObject("ID_GRUP")).collect(toList()));
                        try(ResultSet getIdGrup = mySQLDAO.doQuery(GenericMySQLQuerys.GENERIC_GET_LAST_ID))
                        {
                            idHistoricGrup = getIdGrup.next() ? getIdGrup.getInt(1) : null;
                        }

                        //Obtenim les dades del Origen
                        DadesGrup dadesOrigen = getDadesGrup(llistaGrupsRS.getInt("PLATAFORMA_ORIGEN"), llistaGrupsRS.getInt("ID_CONFIG_ORIGEN"));    
                        
                        if(dadesOrigen == null || dadesOrigen.getDadesConfiguracioGrup() == null)
                        {                            
                            if(dadesOrigen == null)
                            {
                                mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_GRUPS_FINAL, of("ERROR", idHistoricGrup).collect(toList()));
                                mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_ERRORS_INTEGRACIO, of(idHistoricGrup, null, "No s'ha trobat la configuració de l'origen", null, "Plataforma " + llistaGrupsRS.getInt("PLATAFORMA_ORIGEN") + " - Configuració Origen: " + llistaGrupsRS.getInt("ID_CONFIG_ORIGEN")).collect(toList()));
                                LOGGER.error("No s'ha trobat la configuració origen " + llistaGrupsRS.getInt("ID_CONFIG_ORIGEN") + " del grup " + llistaGrupsRS.getObject("NOM_GRUP") + " amb ID " + llistaGrupsRS.getObject("ID_GRUP") + " . Passem a processar el següent");
                            }
                            else
                            {
                                mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_GRUPS_FINAL, of(resultatGrup, idHistoricGrup).collect(toList()));
                                LOGGER.warn("No s'han trobat contactes a per la configuració origen " + llistaGrupsRS.getInt("ID_CONFIG_ORIGEN") + " del grup " + llistaGrupsRS.getObject("NOM_GRUP") + " amb ID " + llistaGrupsRS.getObject("ID_GRUP") + " . Passem a processar el següent");
                            }
                            
                            continue;  
                        }

                        //Obtenim el llistat de configuracions destins a sincronitzar
                        try(ResultSet llistaDestinsRS = mySQLDAO.doQuery(MySQLQuerys.INTEGRACIO_GET_DESTINS_GRUP, of(llistaGrupsRS.getObject("ID_GRUP")).collect(toList())))
                        {
                            while(llistaDestinsRS.next())
                            {
                                mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_DESTINS_INICI, of(idHistoricGrup, llistaDestinsRS.getInt("ID_CONFIGURACIO")).collect(toList()));
                                try(ResultSet getIdDesti = mySQLDAO.doQuery(GenericMySQLQuerys.GENERIC_GET_LAST_ID))
                                {
                                    idHistoricDesti = getIdDesti.next() ? getIdDesti.getInt(1) : null;
                                }

                                LOGGER.info("Processem el destí: Plataforma " + llistaDestinsRS.getInt("ID_PLATAFORMA") + " - Configuració " + llistaDestinsRS.getInt("ID_CONFIGURACIO"));

                                //Obtenim les dades del Destí
                                DadesGrup dadesDesti = getDadesGrup(llistaDestinsRS.getInt("ID_PLATAFORMA"), llistaDestinsRS.getInt("ID_CONFIGURACIO"));
                                if(dadesDesti == null)
                                {
                                    mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_DESTINS_FINAL, of("ERROR", 0, 0, 0, 0, null, idHistoricDesti).collect(toList()));
                                    mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_ERRORS_INTEGRACIO, of(idHistoricGrup, idHistoricDesti, "No s'ha trobat la configuració del destí", null, "Plataforma " + llistaDestinsRS.getInt("ID_PLATAFORMA") + " -  Configuració " + llistaDestinsRS.getInt("ID_CONFIGURACIO")).collect(toList()));

                                    LOGGER.error("No s'han trobat la configuració del destí. Plataforma " + llistaDestinsRS.getInt("ID_PLATAFORMA") + " -  Configuració " + llistaDestinsRS.getInt("ID_CONFIGURACIO") + ". Passem a processar el següent");
                                    continue;
                                }

                                //Sincronitzem l'Origen amb el Destí
                                Map<String, Object> resultatDesti = null;
                                if(dadesOrigen.getIdPlataformaGrup() == 1 && dadesDesti.getIdPlataformaGrup() == 2) resultatDesti = sincronitzarPlataformes(dadesOrigen, dadesDesti);     

                                //Guardem el resutlat al historic de Destins
                                mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_DESTINS_FINAL, of("OK", resultatDesti.get("Altes"), 
                                                                                                       resultatDesti.get("Updates"),
                                                                                                       resultatDesti.get("Baixes"),
                                                                                                       resultatDesti.get("Evitats"),
                                                                                                       resultatDesti.get("Detalls"),
                                                                                                       idHistoricDesti).collect(toList()));

                                LOGGER.info("S'ha acabat de sincronitzar el destí: " + resultatDesti.get("Altes") + " Altes, " + resultatDesti.get("Updates") + " Updates, " + resultatDesti.get("Baixes") + " Baixes y " + resultatDesti.get("Evitats") + " Evitats. Detalls -> " + resultatDesti.get("Detalls"));
                                LOGGER.info("Pot consultar l'històric del destí " + idHistoricDesti);
                            }
                        }
                        catch(Exception e)
                        {
                            mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_DESTINS_FINAL, of("ERROR", 0, 0, 0, 0, null, idHistoricDesti).collect(toList()));
                            mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_ERRORS_INTEGRACIO, of(idHistoricGrup, idHistoricDesti, null, e.getClass() + " -> " + e.getMessage(), Arrays.toString(e.getStackTrace())).collect(toList()));

                            LOGGER.error("Hi ha hagut un error al processar el següent historic Desti: " + idHistoricDesti + "\nEl missatge del error és: ", e);
                        }

                        //Guardem el resutlat al historic de Grups
                        mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_GRUPS_FINAL, of(resultatGrup, idHistoricGrup).collect(toList()));
                        LOGGER.info("S'ha acabat de sincronitzar el grup. Pot consultar l'històric " + idHistoricGrup);
                    }
                    catch(Exception e)
                    {   
                        mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_HISTORIC_GRUPS_FINAL, of("ERROR", idHistoricGrup).collect(toList()));
                        mySQLDAO.doDML(MySQLQuerys.INTEGRACIO_ERRORS_INTEGRACIO, of(idHistoricGrup, null, null, e.getClass() + " -> " + e.getMessage(), Arrays.toString(e.getStackTrace())).collect(toList()));

                        LOGGER.error("Hi ha hagut un error al processar el grup del següent historic: " + idHistoricGrup + "\nEl missatge del error és: ", e);
                    }
                }
            }
            
            resultatProces = Response.status(200).entity("La sincornització ha Finalitzat. Consultar taula HISTORIC_INTEGRACIO_GRUPS per veure el resultat de cada grup").encoding("UTF-8").build();
        }
        catch(Exception e)
        {
            resultatProces = Response.status(Response.Status.INTERNAL_SERVER_ERROR).encoding("UTF-8").entity("S'ha generat un error no conrtolat durant el procés d'IntegracioGrups. \n "
                                                                                                           + "Consultar el log de l'aplicació per més informació").encoding("UTF-8").build();
            LOGGER.error("S'ha generat un error no controlat: ", e);
        }
        finally
        {
            //Si la conexió està oberta, la tanquem
            if(mySQLDAO != null) mySQLDAO.closeConnection();
        }
        
        LOGGER.info("Fi d'el procés d'Integració");
        return resultatProces;
    }
    
    private Map<String,Object> getConfiguracio(int idPlataforma, int idConfiguracio) throws SQLException
    {
        ResultSet configuracioRS;
        Map<String,Object> campsConfiguracio = null;
        
        //Obtenim la informació de la plataforma
        ResultSet plataformaRS = mySQLDAO.doQuery(MySQLQuerys.INTEGRACIO_GET_PLATAFORMA, of(idPlataforma).collect(toList()));
        
        //Si existeix la plataforma continuem
        if(plataformaRS.next())
        {
            String taulaOrigen = plataformaRS.getString("TAULA_CONFIGURACIONS");
            
            //Petita comprovació per evitar SQL Injection    
            if(taulaOrigen.contains(" ")) throw new SQLException("Nom de al taula de configuració origen no és correcte");
            
            //Obtenim la configuració
            configuracioRS = mySQLDAO.doQuery(MySQLQuerys.INTEGRACIO_GET_CONFIGURACIO_SELECT + taulaOrigen + MySQLQuerys.INTEGRACIO_GET_CONFIGURACIO_WHERE, of(idConfiguracio).collect(toList()));
        
            //Obtenim cada valor de la configuració i montem la variable de sortida
            if(configuracioRS.next())
            {
                campsConfiguracio = new HashMap<>();

                ResultSetMetaData metadata = configuracioRS.getMetaData();
                int numColumnes = metadata.getColumnCount();

                for(int i = 1; i <= numColumnes; i++) campsConfiguracio.put(metadata.getColumnName(i), configuracioRS.getObject(i));
            }
        }
 
        return campsConfiguracio;
    }
    
    private DadesGrup getDadesGrup(int idPlataforma, int idConfiguracio) throws SQLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        DadesGrup dadesGrup = null;
        
        //Obtenim la configuració de la plataforma
        Map<String,Object> configuracio = getConfiguracio(idPlataforma, idConfiguracio);
                
        //Pasem a obtenir la informació en base a la configuració obtindguda
        if(configuracio != null && !configuracio.isEmpty())
        {
            //Creem l'objecte amb les dades del grup
            dadesGrup = new DadesGrup(idPlataforma, configuracio, null);
            
            //Obtenim els contactes de la plataforma segons la configuració obtinguda
            RestResponse contactesPlataforma = getContactesPlataforma(configuracio, idPlataforma);
            
             //Insertem els contactes obtinguts
            if(null != contactesPlataforma) dadesGrup.setDadesConfiguracioGrup(contactesPlataforma.getContingutResposta());
        }
                        
        return dadesGrup;
    }
    
    private RestResponse getContactesPlataforma(Map<String,Object> configuracioPlataforma, int idPlataforma) throws SQLException, IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse respostaPlataforma = null;
        
        switch(idPlataforma)
        {
            case 1:  //Salesforce
                    
                salesForceDAO = new SalesforceDAO(configuracioPlataforma.get("ENDPOINT_LOGIN").toString(), configuracioPlataforma.get("USERNAME_SF").toString(), configuracioPlataforma.get("PASSWORD_SF").toString(), configuracioPlataforma.get("CLIENT_ID").toString(), configuracioPlataforma.get("CLIENT_SECRET").toString(), configuracioPlataforma.get("GRANT_TYPE").toString());
                
                respostaPlataforma = salesForceDAO.getInfoContactes(configuracioPlataforma.get("ENDPOINT_REPORT").toString(), configuracioPlataforma.get("REPORT_ID").toString(), configuracioPlataforma.get("CAMP_MAIL").toString(), configuracioPlataforma.get("CAMP_MULTIVALOR").toString());
                
                if(!respostaPlataforma.validateResponse())
                {
                    throw new IOException("Error amb la paltaforma SalesForce: " + respostaPlataforma.getCodiResposta() + " - " + respostaPlataforma.getMissatgeResposta() + " - " + respostaPlataforma.getContingutResposta().getString("RequestError"));
                }
                
                break;
                    
            case 2:  //MailChimp
                
                //Si es Mailchimp, creem l'objecte amb les dades necessáries per la conexió
                mailChimpDAO = new MailChimpDAO(configuracioPlataforma.get("ENDPOINT_MAILCHIMP").toString(), configuracioPlataforma.get("API_KEY").toString());
                
                respostaPlataforma = mailChimpDAO.getInfoContactes(configuracioPlataforma.get("LLISTA_ID").toString());

                if(!respostaPlataforma.validateResponse()) throw new IOException("Error amb la paltaforma MailChimp: " + respostaPlataforma.getCodiResposta() + " - " + respostaPlataforma.getMissatgeResposta() + " - " + respostaPlataforma.getContingutResposta().getString("RequestError"));
                
                break;
                
            default:
                break;
        }
        
        return respostaPlataforma;
    }

    
    private Map<String, Object> sincronitzarPlataformes(DadesGrup dadesOrigen, DadesGrup dadesDesti) throws NoSuchAlgorithmException, IOException, URISyntaxException
    {
        Map<String, Object> resultatSincro = null;
        
        //Mirem quines plataformes Origen-Desti tenim per crida la funció d'integracio corresponent
        switch(dadesOrigen.getIdPlataformaGrup())
        {
            //Salesforce
            case 1:
                switch(dadesDesti.getIdPlataformaGrup())
                {
                    //Salesforce-Mailchimp
                    case 2:
                        resultatSincro = Salesforce_Mailchimp.sincrontizarSF_Mailchimp(dadesOrigen, dadesDesti, mailChimpDAO);
                        break;
                }
                break;
        }
        
        return resultatSincro;
    }
    
    private void inicialitzar() throws FileNotFoundException, IOException
    {
        //Si falta alguan dada que no s'hagi obtingut al inicar l'aplicació, intentem obtenir-la ara.
        if(System.getProperty("urlMySQL") == null)
        {
            InputStream input = new FileInputStream("/uvic/integraciogrups/Connexions.properties");
            
            Properties prop = new Properties();
            prop.load(input);

            System.setProperty("urlMySQL", prop.getProperty("urlMySQL"));
            System.setProperty("usuariMySQL", prop.getProperty("usuariMySQL"));
            System.setProperty("passwordMySQL", prop.getProperty("passwordMySQL"));            
        }
        
        //Inicialitzem el Log4j2
        if(System.getProperty("log4j.configurationFile") == null)
        {
            System.setProperty("log4j.configurationFile", "/uvic/integraciogrups/log4j2.xml");
        }
    }
}