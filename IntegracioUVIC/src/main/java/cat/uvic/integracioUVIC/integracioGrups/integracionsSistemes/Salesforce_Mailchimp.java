/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.uvic.integracioUVIC.integracioGrups.integracionsSistemes;

import cat.uvic.integracioUVIC.integracioGrups.daos.MailChimpDAO;
import cat.uvic.integracioUVIC.integracioGrups.pojos.DadesGrup;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import cat.uvic.integracioUVIC.transversal.utils.MailChimpUtils;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Masu
 */
public class Salesforce_Mailchimp
{
    private static final Logger LOGGER = LogManager.getLogger(Salesforce_Mailchimp.class.getName());
    
    public static Map<String, Object> sincrontizarSF_Mailchimp (DadesGrup dadesOrigen, DadesGrup dadesDesti, MailChimpDAO mailChimpDAO) throws NoSuchAlgorithmException, IOException, URISyntaxException
    {
        //Obtenim dades de les configuracions
        String campMail         = dadesOrigen.getConfiguracioGrup().get("CAMP_MAIL").toString();
        String campMultivalorSF = dadesOrigen.getConfiguracioGrup().get("CAMP_MULTIVALOR").toString();
        
        String llistaId         = dadesDesti.getConfiguracioGrup().get("LLISTA_ID").toString();
        String urlMailchimp     = dadesDesti.getConfiguracioGrup().get("ENDPOINT_MAILCHIMP").toString();
        String apiKeyMailchimp  = dadesDesti.getConfiguracioGrup().get("API_KEY").toString();
        String nomGrupMC        = dadesDesti.getConfiguracioGrup().get("NOM_GRUP_MULTIVALOR").toString();
        
        Map<String, String> valorsGrupMC;
        
        AtomicInteger altes     = new AtomicInteger(0);
        AtomicInteger updates   = new AtomicInteger(0);
        AtomicInteger baixes    = new AtomicInteger(0);
        AtomicInteger evitats   = new AtomicInteger(0);
        
        MailChimpDAO mcConsumer = new MailChimpDAO(urlMailchimp, apiKeyMailchimp);
        
        //Comparem els camps i creem els que no existeixin
        JsonObject campsMC  = mailChimpDAO.getMergeFields(llistaId).getContingutResposta();
        JsonObject campsSF  = (JsonObject)dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").parallelStream().filter(t -> ((JsonObject)t).containsKey("labelCamps")).collect(toList()).get(0);
                
        JsonArray campsFaltants = compararCampsSF_Mailchimp(campsSF, campsMC);
        
        //Si trovem que falten camps, els agreguem
        if(!campsFaltants.isEmpty())
        {
            //Comencem a montar la estructura de Batch
            JsonArrayBuilder jsonOperationsCamps = Json.createArrayBuilder();            
            campsFaltants.stream().forEach(t -> MailChimpUtils.afegirCampBatch(t, jsonOperationsCamps, llistaId));

            RestResponse resultatCamps = mcConsumer.sendBatch(Json.createObjectBuilder().add("operations", jsonOperationsCamps).build());
            
            if(resultatCamps.validateResponse()) LOGGER.info("S'ha enviat el batch " + resultatCamps.getContingutResposta().getString("id") + " per crear els següents camps a MailChimp: " + campsFaltants.toString());
            else                                 LOGGER.error("Hi h hagut un error a l'hora de crear els nous camps a MailChimp. Resposta: " + resultatCamps.getCodiResposta() + " - " + resultatCamps.getMissatgeResposta());
        }
        
        //Mirem si hi ha algun camp multivalor.
        if(campMultivalorSF != null)
        {           
            //Obtenim el id del grup a MC. Si no existeix es crea.
            String idGrupMailchimp = crearObtenirGrupMC (mailChimpDAO, llistaId, nomGrupMC);
            
            //Obtenim els valors del grup de MC y els que Arriven de SF (distinct)
            List<String> multivalorMC = mailChimpDAO.getGroupValues(llistaId, idGrupMailchimp).getContingutResposta().getJsonArray("interests").parallelStream().map(t -> ((JsonObject)t).getString("name")).collect(toList());
            List<String> multivalorSF = dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").parallelStream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).filter(t -> !"-".equals(((JsonObject)t).getString(campMultivalorSF)) ).map(t -> ((JsonObject)t).getString(campMultivalorSF)).distinct().collect(toList());
            
            //Eliminem de la llista de SF, els que ja estan creats a Mailchimp.
            multivalorSF.removeAll(multivalorMC);
            
            //Si queden valors perquè no està creat a MC, el creem
            if(!multivalorSF.isEmpty()) crearValorsGrupMC(mailChimpDAO, llistaId,idGrupMailchimp, multivalorSF);

            //Obtenim tots els valors possibles del grup a Mailchimp  
            valorsGrupMC = mailChimpDAO.getGroupValues(llistaId, idGrupMailchimp).getContingutResposta().getJsonArray("interests").parallelStream().collect(Collectors.toMap(t -> ((JsonObject)t).getString("name"), t -> ((JsonObject)t).getString("id")));
        }
        else
        {
            valorsGrupMC = null;
        }
        
        //Obtenim les llistes de contactes de cada entorn       
        List<String>        emailsSF = dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").parallelStream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).map(t -> ((JsonObject)t).getString(campMail).toLowerCase()).collect(toList());
        Map<String, String> emailsMC = dadesDesti.getDadesConfiguracioGrup().getJsonArray("members").parallelStream().collect(Collectors.toMap(t -> ((JsonObject)t).getString("email_address").toLowerCase(), t -> ((JsonObject)t).getString("status")));         

        //Comencem a montar la estructura de Batch
        JsonArrayBuilder jsonOperations = Json.createArrayBuilder();
        
        
        //Creem a Mailchimp les noves contes que no existien
        dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").stream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).filter(t -> !emailsMC.containsKey(((JsonObject)t).getString(campMail).toLowerCase())).map(t -> ((JsonObject)t).getString(campMail).toLowerCase()).distinct().collect(toList()).forEach(contacte ->
        {
            List valorsGrupContacte = dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").parallelStream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).filter(t -> contacte.equals(((JsonObject)t).getString(campMail).toLowerCase())).map(t -> ((JsonObject)t).getString(campMultivalorSF)).collect(toList());
            
            Optional<JsonValue> datosUsuario = dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").parallelStream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).filter(t -> contacte.equals(((JsonObject)t).getString(campMail).toLowerCase())).findAny();
            if(datosUsuario.isPresent())
            {
                MailChimpUtils.afegirContacteBatch("POST", datosUsuario.get(), jsonOperations, llistaId, campMail, "subscribed", campMultivalorSF, valorsGrupMC, valorsGrupContacte);
                altes.getAndIncrement();
            }
        });    
            
        
        //Actualizem les contes que ja existien
        dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").stream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).filter(t ->  emailsMC.containsKey(((JsonObject)t).getString(campMail).toLowerCase())).map(t -> ((JsonObject)t).getString(campMail).toLowerCase()).distinct().collect(toList()).forEach(contacte ->
        {
            List valorsGrupContacte = dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").parallelStream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).filter(t -> contacte.equals(((JsonObject)t).getString(campMail).toLowerCase())).map(t -> ((JsonObject)t).getString(campMultivalorSF)).collect(toList());    

            Optional<JsonValue> datosUsuario = dadesOrigen.getDadesConfiguracioGrup().getJsonArray("llistaContactes").parallelStream().filter(t -> !((JsonObject)t).containsKey("labelCamps") && !((JsonObject)t).containsKey("campMultivalor")).filter(t -> contacte.equals(((JsonObject)t).getString(campMail).toLowerCase())).findAny();
            if(datosUsuario.isPresent())
            {
                if("subscribed".equals(emailsMC.get(((JsonObject)datosUsuario.get()).getString(campMail).toLowerCase())))
                {
                    MailChimpUtils.afegirContacteBatch("PUT", datosUsuario.get(), jsonOperations, llistaId, campMail, "subscribed", campMultivalorSF, valorsGrupMC, valorsGrupContacte);
                    updates.getAndIncrement();
                }
                else
                {
                    evitats.getAndIncrement();
                }
            }
        });

        
        //Fem un Unsubscribe de les contes que ja no existeixen a SF
        dadesDesti.getDadesConfiguracioGrup().getJsonArray("members").stream().filter(t -> (!emailsSF.contains(((JsonObject)t).getString("email_address").toLowerCase()) && "subscribed".equals(((JsonObject)t).getString("status")))).forEach(t ->
        {
            MailChimpUtils.afegirContacteBatch("PUT", t, jsonOperations, llistaId, "email_address", "unsubscribed", campMultivalorSF, valorsGrupMC, null);
            baixes.getAndIncrement();
        });
         
        //Executem el Batch
        JsonArrayBuilder batch = null;
        
        int contadorBloc = 0;
        String idsBatch = "";
        
        for(JsonValue operacio : jsonOperations.build())
        {
            if(operacio != null)
            {
                if(contadorBloc == 0 || batch == null) batch = Json.createArrayBuilder();

                batch.add(operacio);
                contadorBloc++;

                if(contadorBloc == 2000)
                {
                    RestResponse resultatBatch = mcConsumer.sendBatch(Json.createObjectBuilder().add("operations", batch).build());
                    if(resultatBatch.validateResponse())
                    {
                        LOGGER.info("ID del Batch de la operació: " + resultatBatch.getContingutResposta().getString("id"));
                        idsBatch = idsBatch + resultatBatch.getContingutResposta().getString("id") + ", ";
                    }
                    contadorBloc = 0;
                }
            }
        }
        
        Map<String, Object> resultatDesti = new HashMap();
        
        if(contadorBloc != 0)
        {
            RestResponse resultat = mcConsumer.sendBatch(Json.createObjectBuilder().add("operations", batch).build());
            resultatDesti.put("Detalls",  resultat.validateResponse() ? "IDs del Batch de la operació: " + idsBatch + resultat.getContingutResposta().getString("id") : null);
        }
    
        //Preparem la resposta
        
        resultatDesti.put("Altes", altes.get());
        resultatDesti.put("Updates", updates.get());
        resultatDesti.put("Baixes", baixes.get());
        resultatDesti.put("Evitats", evitats.get());
        
        return resultatDesti;
    }

    private static JsonArray compararCampsSF_Mailchimp(JsonObject campsSF, JsonObject campsMC)
    {
        JsonArrayBuilder llistaCampsFaltants = Json.createArrayBuilder();

        campsSF.getJsonObject("labelCamps").forEach((String k, JsonValue v) ->
        { 
            String valor = (k.length() > 10 ? k.substring(0,10) : k).toUpperCase();

            if(0 == campsMC.getJsonArray("merge_fields").parallelStream().filter(t -> ((JsonObject)t).getString("tag").equals(valor) || valor.equals("EMAIL")).count())
            {
                llistaCampsFaltants.add(Json.createObjectBuilder().add(valor, v));
            }
        });

        return llistaCampsFaltants.build();
    }
    
    //Retorna el id del grup a Mailchimp. Si no existeix, el crea
    private static String crearObtenirGrupMC (MailChimpDAO mailChimpDAO, String llistaId, String nomGrupMC) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        //Comprovem si el grup existeix.          
        List grupsMC = mailChimpDAO.getGroups(llistaId).getContingutResposta().getJsonArray("categories").parallelStream().filter(t -> nomGrupMC.equals(((JsonObject)t).getString("title"))).collect(toList());

        //Si el grup no existeix a MC, el creem. Si ja existeix, obtenim el id del grup.
        if(grupsMC.isEmpty())
        {
            RestResponse resultatCreacio = mailChimpDAO.createGroup(llistaId, nomGrupMC, "checkboxes");

            if(resultatCreacio.validateResponse())
            {
                return resultatCreacio.getContingutResposta().getString("id");
            }
            else
            {
                LOGGER.error("Hi h hagut un error a l'hora de crear el grup de camps multivalor a MailChimp. Resposta: " + resultatCreacio.getCodiResposta() + " - " + resultatCreacio.getMissatgeResposta());
                return null;
            }
        }
        else
        {
            return ((JsonObject)grupsMC.get(0)).getString("id");
        }
    }
    
    private static void crearValorsGrupMC(MailChimpDAO mailChimpDAO, String llistaId, String idGrupMailchimp, List<String> multivalorSF)
    {
        multivalorSF.stream().forEach(t -> 
        {
            try 
            {
                RestResponse resultatValors = mailChimpDAO.createGroupValue(llistaId, idGrupMailchimp, t);
                if(!resultatValors.validateResponse()) LOGGER.error("Hi h hagut un error a l'hora de crear el nou valor " + t + " al grup " + idGrupMailchimp + " de MailChimp. Resposta: " + resultatValors.getCodiResposta() + " - " + resultatValors.getMissatgeResposta());
            } 
            catch (IOException | URISyntaxException | NoSuchAlgorithmException ex)
            {
                LOGGER.error("Hi h hagut un error a l'hora de crear els nous valors al grup de MailChimp. Error: " + ex.getMessage());
            }
        });
    }
}
