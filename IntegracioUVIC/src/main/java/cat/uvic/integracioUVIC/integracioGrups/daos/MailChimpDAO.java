package cat.uvic.integracioUVIC.integracioGrups.daos;

import cat.uvic.integracioUVIC.transversal.APIs.MailChimpRestAPI;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MailChimpDAO
{
    private String apiKeyMailchimp;
    private String urlMailchimp;    
    
    private static final Logger LOGGER = LogManager.getLogger(MailChimpDAO.class);
    
    //Constructor con todos los elementos
    public MailChimpDAO(String urlMailchimp, String apiKeyMailchimp)
    {
        this.apiKeyMailchimp = apiKeyMailchimp;
        this.urlMailchimp = urlMailchimp;
    }
    
    public RestResponse getInfoContactes(String llistaId) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        return getInfoContactes(llistaId, null);
    }
    
    public RestResponse getInfoContactes(String llistaId, List<Pair<String, String>> parametres) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        if(parametres == null)
        {
            parametres= new ArrayList<>();
            parametres.add(new Pair("fields", "total_items,members.email_address,members.status"));
        }
        
        LOGGER.debug("Obtenem la llist de MailChimp: " + llistaId);
        return MailChimpRestAPI.getInfoList(urlMailchimp, apiKeyMailchimp, llistaId, parametres);
    }
    
    public RestResponse sendBatch(JsonObject contingut) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        LOGGER.debug("Enviem un Batch de MailChimp: " + contingut);
        return MailChimpRestAPI.executeBatch(urlMailchimp, apiKeyMailchimp, null, contingut);
    }
    
    public RestResponse getMergeFields(String llistaId) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        LOGGER.debug("Obtenim el llistat de Camps de la llista " + llistaId + " de MailChimp");
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("count", "30"));
        parametres.add(new Pair("fields", "merge_fields.tag"));
                
        return MailChimpRestAPI.getListFields(urlMailchimp, apiKeyMailchimp, llistaId, parametres, null);
    }
   
    public RestResponse createGroup(String llistaId, String nomGrup, String tipusGrup) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        LOGGER.debug("Creem un grupo a llista " + llistaId + " de MailChimp");
        JsonObjectBuilder contingut = Json.createObjectBuilder();
        contingut.add("title", nomGrup);
        contingut.add("type", tipusGrup);
                
        return MailChimpRestAPI.createGroup(urlMailchimp, apiKeyMailchimp, llistaId, null, contingut.build());
    }
    
    public RestResponse getGroups(String llistaId) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        LOGGER.debug("Obtenim el llistat de Grups de la llista " + llistaId + " de MailChimp");
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("fields", "categories.title,categories.id"));
                
        return MailChimpRestAPI.getGroups(urlMailchimp, apiKeyMailchimp, llistaId, parametres, null);
    }

    public RestResponse getGroupValues(String llistaId, String grupId) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        LOGGER.debug("Obtenim el llistat de valors del Grup de la llista " + llistaId + " i el grup " + grupId + " de MailChimp");
        List<Pair<String, String>> parametres = new ArrayList<>();
        parametres.add(new Pair("fields", "interests.name,interests.id"));
        parametres.add(new Pair("count", "100000"));
                
        return MailChimpRestAPI.getGroupValues(urlMailchimp, apiKeyMailchimp, llistaId, grupId, parametres, null);
    }
    
    public RestResponse createGroupValue(String llistaId, String grupId, String nomCamp) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        LOGGER.debug("Afegim un nou valor al Grup de la llista " + llistaId + " i el grup " + grupId + " de MailChimp");
        JsonObjectBuilder contingut = Json.createObjectBuilder();
        contingut.add("name", nomCamp);
                
        return MailChimpRestAPI.createGroupValues(urlMailchimp, apiKeyMailchimp, llistaId, grupId, null, contingut.build());
    }
    
    
/////////////////////////////////////////////////////             GETTERS I SETTERS                //////////////////////////////////////////////      
    
    public String getApiKeyMailchimp()
    {
        return apiKeyMailchimp;
    }

    public void setApiKeyMailchimp(String apiKeyMailchimp)
    {
        this.apiKeyMailchimp = apiKeyMailchimp;
    }

    public String getUrlMailchimp()
    {
        return urlMailchimp;
    }

    public void setUrlMailchimp(String urlMailchimp)
    {
        this.urlMailchimp = urlMailchimp;
    }
}
