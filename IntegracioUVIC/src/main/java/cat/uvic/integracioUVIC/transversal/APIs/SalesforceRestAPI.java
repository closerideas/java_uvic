package cat.uvic.integracioUVIC.transversal.APIs;

import cat.uvic.integracioUVIC.transversal.connectors.HttpConnector;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import javax.json.JsonObject;


public class SalesforceRestAPI
{
    private static String tokenType;
    private static String accessToken;
    
    private enum RestMethods{GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE}
    
    public static RestResponse doLogin(String url, String usuari, String password, String clientId, String clientSecret, String grantType) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> parametres = new ArrayList<>();
        
        parametres.add(new Pair("username",      usuari));
        parametres.add(new Pair("password",      password));
        parametres.add(new Pair("client_id",     clientId));
        parametres.add(new Pair("client_secret", clientSecret));
        parametres.add(new Pair("grant_type",    grantType));
        
        RestResponse resposta = HttpConnector.RestCall(RestMethods.POST.name(), url, parametres);
        
        tokenType  = resposta.getContingutResposta().getString("token_type");
        accessToken = resposta.getContingutResposta().getString("access_token");
        
        return resposta;
    }
    
    public static boolean isLoged(String tokenType, String accessToken)
    {
        return SalesforceRestAPI.tokenType   != null 
            && SalesforceRestAPI.accessToken != null 
            && SalesforceRestAPI.tokenType.equals(tokenType) 
            && SalesforceRestAPI.accessToken.equals(accessToken);
    }
    
    public static RestResponse getReport(String url, String reportId) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        return getReport(url, reportId, tokenType, accessToken);
    }
        
    public static RestResponse getReport(String url, String reportId, String tokenType, String accessToken) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", tokenType + " " + accessToken));
                
        return HttpConnector.RestCall(RestMethods.POST.name(), url + reportId, null, null, headers);
    }
    
    public static RestResponse getReportFiltered(String url, String reportId, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        return getReportFiltered(url, reportId, tokenType, accessToken, content);
    }
        
    public static RestResponse getReportFiltered(String url, String reportId, String tokenType, String accessToken, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", tokenType + " " + accessToken));
                
        return HttpConnector.RestCall(RestMethods.POST.name(), url + reportId, null, content, headers);
    }
    
    public static RestResponse getReportMetadata(String url, String reportId) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {     
        return getReportMetadata(url, reportId, tokenType, accessToken);
    }
    
    public static RestResponse getReportMetadata(String url, String reportId, String tokenType, String accessToken) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", tokenType + " " + accessToken));
                
        return HttpConnector.RestCall(RestMethods.GET.name(), url + reportId + "/describe", null, null, headers);
    }
}
