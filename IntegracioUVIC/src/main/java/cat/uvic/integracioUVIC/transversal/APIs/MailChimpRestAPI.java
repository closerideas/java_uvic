package cat.uvic.integracioUVIC.transversal.APIs;

import cat.uvic.integracioUVIC.transversal.connectors.HttpConnector;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.xml.bind.DatatypeConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Masu
 */
public class MailChimpRestAPI
{    
    private static final Logger LOGGER = LogManager.getLogger(MailChimpRestAPI.class.getName());
    private enum RestMethods{GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE}
    
    public static RestResponse getInfoList(String url, String apiKey, String llistaId, List<Pair<String, String>> parameters) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        RestResponse resultadoIteracion = null;
        RestResponse resultadoFinal = new RestResponse();
        List<Pair<String, String>> headers = new ArrayList<>();
        
        int offset = 0;
        int tamanyoPaginacion = 5000;
               
        headers.add(new Pair("Authorization", apiKey));
        JsonArrayBuilder llistaMembers  = Json.createArrayBuilder();
         
        while(resultadoIteracion == null || resultadoIteracion.getContingutResposta().getJsonArray("members").size() == tamanyoPaginacion)
        {
            LOGGER.info("Offset: " + offset);
            List<Pair<String, String>> parametresIteracio = new ArrayList<>();
            
            parameters.stream().forEach((parameter) -> {parametresIteracio.add(new Pair(parameter.getKey(), parameter.getValue()));});
            
            parametresIteracio.add(new Pair("count", tamanyoPaginacion));
            parametresIteracio.add(new Pair("offset", offset));
            
            resultadoIteracion = HttpConnector.RestCall(RestMethods.GET.name(), url + "lists/" + llistaId + "/members", parametresIteracio, null, headers);
        
            resultadoFinal.setCodiResposta(resultadoIteracion.getCodiResposta());
            resultadoFinal.setMissatgeResposta(resultadoIteracion.getMissatgeResposta());
            
            if(resultadoIteracion.validateResponse())
            {
                LOGGER.info("Juntem contactes");
                
                for(JsonValue member : resultadoIteracion.getContingutResposta().getJsonArray("members"))
                {
                    llistaMembers.add(member);
                }
            }
            else
            {
                break;
            }
            
            LOGGER.info("Contactes obtinguts per aquesta Iteració: " + resultadoIteracion.getContingutResposta().getJsonArray("members").size());
            offset = offset + tamanyoPaginacion;
        }
        
        resultadoFinal.setContingutResposta(Json.createObjectBuilder().add("members", llistaMembers.build()).build());
        LOGGER.info("Contactes totals obtinguts: " + resultadoFinal.getContingutResposta().getJsonArray("members").size());

        return resultadoFinal;
    }
    
    public static RestResponse addNewContactToList(String url, String apiKey, String llistaId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.POST.name(), url + "lists/" + llistaId + "/members", parameters, content, headers);
    }
    
    public static RestResponse modifyContactoFromList(String url, String apiKey, String llistaId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        MessageDigest md = MessageDigest.getInstance("MD5");
        
        md.update(content.getString("email_address").toLowerCase().getBytes());
        String emailHash = DatatypeConverter.printHexBinary(md.digest()).toUpperCase();
        
        return HttpConnector.RestCall(RestMethods.PUT.name(), url + "lists/" + llistaId + "/members/" + emailHash, parameters, content, headers);
    }
    
    public static RestResponse executeBatch(String url, String apiKey, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.POST.name(), url + "batches", parameters, content, headers);
    }
    
    public static RestResponse getListFields(String url, String apiKey, String llistaId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.GET.name(), url + "lists/" + llistaId + "/merge-fields", parameters, content, headers);
    }
    
    public static RestResponse updateListFields(String url, String apiKey, String llistaId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.POST.name(), url + "lists/" + llistaId + "/merge-fields", parameters, content, headers);
    }

    public static RestResponse createGroup(String url, String apiKey, String llistaId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.POST.name(), url + "lists/" + llistaId + "/interest-categories", parameters, content, headers);
    }
    
    public static RestResponse getGroups(String url, String apiKey, String llistaId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.GET.name(), url + "lists/" + llistaId + "/interest-categories", parameters, content, headers);
    }
    
    public static RestResponse getGroupValues(String url, String apiKey, String llistaId, String grupId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.GET.name(), url + "lists/" + llistaId + "/interest-categories/" + grupId + "/interests/", parameters, content, headers);
    }
    
    public static RestResponse createGroupValues(String url, String apiKey, String llistaId, String grupId, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        List<Pair<String, String>> headers = new ArrayList<>();
        headers.add(new Pair("Authorization", apiKey));
        
        return HttpConnector.RestCall(RestMethods.POST.name(), url + "lists/" + llistaId + "/interest-categories/" + grupId + "/interests/", parameters, content, headers);
    }
}
