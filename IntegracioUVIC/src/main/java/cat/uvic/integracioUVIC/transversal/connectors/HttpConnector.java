package cat.uvic.integracioUVIC.transversal.connectors;

import cat.uvic.integracioUVIC.integracioGrups.operacionsServei.IntegracioGrups;
import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Scanner;
import javafx.util.Pair;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonStructure;
import javax.json.stream.JsonParsingException;
import javax.net.ssl.HttpsURLConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/********************************************************************************************************************************************************************
*                                                                                                                                                                   *
*   Clase amb diferents métodes estatics per gestionar les peticions de tipus HTTP per poder consumir serveis REST.                                                 *
*                                                                                                                                                                   *
*   @author Marc Masoliver {Closerideas}                                                                                                                            *
*                                                                                                                                                                   *
*********************************************************************************************************************************************************************/
public class HttpConnector
{   
    /* Default Headers */
    private final static String AGENT    = "Apache-HttpClient/4.5.3 (Java/1.8.0_121)";
    private final static String TYPE     = "application/json; charset=UTF-8";
    
    private static final Logger LOGGER = LogManager.getLogger(IntegracioGrups.class);
    
    /********************************************************************************************************************************************************************
    *                                                                                                                                                                   *
    *   Sobrecárrega del métode:<br> RestCall(String, String, List, JsonObject, List)                                                                                   *
    *                                                                                                                                                                   *
    *   @param method tipus de petició que es realitzará <br><ul><li>GET</li><li>POST</li><li>HEAD</li><li>OPTIONS</li><li>PUT</li><li>DELETE</li><li>TRACE</li></ul>   *
    *   @param url    URL on s'enviará la petició                                                                                                                       *
    *                                                                                                                                                                   *
    *   @return cat.uvic.integraciogrups.pojos.RestResponse                                                                                                             *
    *                                                                                                                                                                   *
    *   @throws java.io.IOException                                                                                                                                     *
    *   @throws java.net.URISyntaxException                                                                                                                             *
    *   @throws java.security.NoSuchAlgorithmException                                                                                                                  *    
    *                                                                                                                                                                   *
    *********************************************************************************************************************************************************************/
   
    public static RestResponse RestCall(String method, String url) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        return RestCall(method, url, null, null, null);
    }
  
    
    /************************************************************************************************************************************************************************
    *                                                                                                                                                                       *
    *   Sobrecárrega del métode:<br> RestCall(String, String, List, JsonObject, List)                                                                                       *
    *                                                                                                                                                                       *
    *   @param method     tipus de petició que es realitzará <br><ul><li>GET</li><li>POST</li><li>HEAD</li><li>OPTIONS</li><li>PUT</li><li>DELETE</li><li>TRACE</li></ul>   *
    *   @param url        URL on s'enviará la Request                                                                                                                       *
    *   @param parameters llistat de parámetres que s'afegirán a la URL de la Request                                                                                       *
    *                                                                                                                                                                       *
    *   @return cat.uvic.integraciogrups.pojos.RestResponse                                                                                                                 *
    *                                                                                                                                                                       *
    *   @throws java.io.IOException                                                                                                                                         *
    *   @throws java.net.URISyntaxException                                                                                                                                 *
    *   @throws java.security.NoSuchAlgorithmException                                                                                                                      *
    *                                                                                                                                                                       *
    *************************************************************************************************************************************************************************/
    
    public static RestResponse RestCall(String method, String url, List<Pair<String, String>> parameters) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        return RestCall(method, url, parameters, null, null);
    }
 
    
    /************************************************************************************************************************************************************************
    *                                                                                                                                                                       *
    *   Sobrecárrega del métode:<br> RestCall(String, String, List, JsonObject, List)                                                                                       *
    *                                                                                                                                                                       *
    *   @param method     tipus de Request que es realitzará <br><ul><li>GET</li><li>POST</li><li>HEAD</li><li>OPTIONS</li><li>PUT</li><li>DELETE</li><li>TRACE</li></ul>   *
    *   @param url        URL on s'enviará la Request                                                                                                                       *
    *   @param parameters llistat de parámetres que s'afegirán a la URL de la Request                                                                                       *
    *   @param content    Json que s'enviarà com a contigut a les Request POST, PUT...                                                                                      *
    *                                                                                                                                                                       *
    *   @return cat.uvic.integraciogrups.pojos.RestResponse                                                                                                                 *
    *                                                                                                                                                                       *
    *   @throws java.io.IOException                                                                                                                                         *
    *   @throws java.net.URISyntaxException                                                                                                                                 *
    *   @throws java.security.NoSuchAlgorithmException                                                                                                                      *     
    *                                                                                                                                                                       *
    ************************************************************************************************************************************************************************/
    
    public static RestResponse RestCall(String method, String url, List<Pair<String, String>> parameters, JsonObject content) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {
        return RestCall(method, url, parameters, content, null);
    }
    
    
    /************************************************************************************************************************************************************************
    *                                                                                                                                                                       *
    *   Funció que realitza les request de tipus HTTP i HTTPS per consumir serveis REST.                                                                                    *
    *                                                                                                                                                                       *
    *   @param method     tipus de Request que es realitzará <br><ul><li>GET</li><li>POST</li><li>HEAD</li><li>OPTIONS</li><li>PUT</li><li>DELETE</li><li>TRACE</li></ul>   *
    *   @param url        URL on s'enviará la Request                                                                                                                       *
    *   @param parameters llistat de parámetres que s'afegirán a la URL de la Request                                                                                       *
    *   @param content    Json que s'enviarà com a contigut a les Request POST, PUT...                                                                                      *
    *   @param headers    llistat de headers que es configurarán a la Request                                                                                               *
    *                                                                                                                                                                       *
    *   @return cat.uvic.integraciogrups.pojos.RestResponse                                                                                                                 *
    *                                                                                                                                                                       *
    *   @throws java.io.IOException                                                                                                                                         *
    *   @throws java.net.URISyntaxException                                                                                                                                 *
    *   @throws java.security.NoSuchAlgorithmException                                                                                                                      *     
    *                                                                                                                                                                       *
    ************************************************************************************************************************************************************************/
    
    public static RestResponse RestCall(String method, String url, List<Pair<String, String>> parameters, JsonObject content, List<Pair<String, String>> headers) throws IOException, URISyntaxException, NoSuchAlgorithmException
    {   
        HttpURLConnection connection = null;
        
        //Preparem la URL
        URL urlRequest = montarURL(url, parameters);
        
        try
        {   
            //Obtneim la connexió configurada
            connection = configurarConexio(urlRequest, method, headers, content);

            //Obtenim el Codi i el missatge de resposta.
            int codiResposta = connection.getResponseCode();

            RestResponse resposta = new RestResponse();    
            resposta.setCodiResposta(String.valueOf(codiResposta));
            resposta.setMissatgeResposta(connection.getResponseMessage());

            //Si la petició ha anat correctament preparem la sortida
            if (200 <= codiResposta && codiResposta < 300) 
            {
                //Obtenim el content de resposta
                InputStream responseStream = connection.getInputStream();
                
                //Si hi ha dades, les convertim a Json
                if(null != responseStream)
                {
                    JsonStructure jsonReaded = Json.createReader(responseStream).read();

                    try
                    {
                        if(jsonReaded instanceof JsonArray) resposta.setContingutResposta(((JsonArray)jsonReaded).getJsonObject(0));
                        else                                resposta.setContingutResposta((JsonObject)jsonReaded);
                    }
                    catch(JsonParsingException jpe)
                    {
                        //Si la petició ha anat correctament però ha donat error al obtenir el contingut, escalem la excepció
                        if(200 <= codiResposta && codiResposta < 300) throw jpe; 
                    }    
                }
            }
            else
            {
                Scanner s = new Scanner(connection.getErrorStream()).useDelimiter("\\A");
                resposta.setContingutResposta(Json.createObjectBuilder().add("RequestError", s.hasNext() ? s.next() : "").build());
            }

            return resposta;
        }
        finally
        {
            if(connection != null)
            {
                try{connection.disconnect();}
                catch(Exception e){}
            }
        }
    }
    
    
/////////////////////////////////////////////////////             MÈTODES PRIVATS                 //////////////////////////////////////////////  
    
    
    
    /************************************************************************************************************
    *****         Funció privada per encapsular la formació finanl de la URL + llistat de parámetres        *****
    *************************************************************************************************************/
    
    private static URL montarURL(String url, List<Pair<String, String>> parameters) throws URISyntaxException, MalformedURLException
    {
        URI uriRequest = new URI(url);
        
        //Si la URL original no té parámetres, agreguem el carácter "?"
        String query = uriRequest.getQuery() == null ? "?" : uriRequest.getQuery();
        
        //Concatenem els parámetres
        if(parameters != null)
        {
            for(Pair parameter : parameters) query = query + parameter.getKey() + "=" + parameter.getValue() + "&";
        }

        //Montem la URL completa. URI + Query
        return new URL(uriRequest.toString() + query.substring(0, query.length() - 1));
    }
    
    
    /************************************************************************************************************
    *****                        Funció privada per crear i configurar la connexió                          *****
    *************************************************************************************************************/
    
    private static HttpURLConnection configurarConexio(URL urlRequest, String method, List<Pair<String, String>> headers, JsonObject content) throws IOException
    {
        //Obrim la conexió segons el tipus de protocol
        HttpURLConnection connection = "https".equals(urlRequest.getProtocol()) ? (HttpsURLConnection)urlRequest.openConnection() : (HttpURLConnection)urlRequest.openConnection();
        
        //Configurem el métode de la petició (GET, POST, PUT...)
        connection.setRequestMethod(method);
        
        //Configurem els Header per defecte
        connection.setRequestProperty("User-Agent", AGENT);
        connection.setRequestProperty("Content-Type", TYPE);

        //Afegim els Headers personalitzats
        if(headers != null) headers.forEach((Pair<String, String> header) -> connection.setRequestProperty(header.getKey(), header.getValue()));

        //Guardem el content de la petició si és necessari
        if(!"GET".equalsIgnoreCase(method))
        {
            connection.setDoOutput(true);
            connection.getOutputStream().write(content != null ? content.toString().getBytes("UTF-8") : "".getBytes("UTF-8"));
        }
        
        return connection;
    }
}