package cat.uvic.integracioUVIC.transversal.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.xml.bind.DatatypeConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Masu
 */
public class MailChimpUtils
{
    private static final Logger LOGGER = LogManager.getLogger(MailChimpUtils.class);
    
    public static String getMailHash(String mail) throws NoSuchAlgorithmException
    {

            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(mail.toLowerCase().getBytes());
            
            return DatatypeConverter.printHexBinary(md.digest()).toUpperCase();
    }
    
    public static JsonArrayBuilder afegirContacteBatch(String action, JsonValue contacte, JsonArrayBuilder jsonOperacionsBatch, String llistaID, String campMail, String subscribed, String campMultivalorSF, Map<String, String> valorsGrupMC, List valorsGrupContacte)
    {
        String emailHash = "";
        JsonObject dadesContacte = (JsonObject)contacte;
        
        //Obtenim el Hash del correu
        if(action.equals("PUT"))
        {
            try
            {
                emailHash = "/" + getMailHash(((JsonObject)contacte).getString(campMail));
            }
            catch(NoSuchAlgorithmException nsae)
            {
                LOGGER.error("Hi ha hagut un error a l'hora d'obtenir el hash de l'usuari " + campMail + ". No es podrá actualitzar");
                return jsonOperacionsBatch;
            }           
        }
        
        //Preparem el body del batch
        JsonObjectBuilder bodyBatch = Json.createObjectBuilder().add("email_address", ((JsonObject)contacte).getString(campMail).toLowerCase()).add("status", subscribed);
        
        //Si es un contacte existent, agreguem els merge_fields
        if("subscribed".equals(subscribed)) 
        {
            JsonObjectBuilder mergeFieldsParsed = Json.createObjectBuilder();
            
            //Validem que el camp no tingui una longitud superior a 255 (Restricció de MailChimp
            dadesContacte.forEach((k,v) -> {
                                                if(!k.equals(campMultivalorSF))         //No posem el camp multivalor
                                                {
                                                    //Limitem el nom del camp a 10 i en majúscules
                                                    String nomCamp   = (k.length() > 10 ? k.substring(0,10) : k).toUpperCase();
                                                    mergeFieldsParsed.add(nomCamp, v);
                                                }
                                           });
            
            bodyBatch.add("merge_fields", mergeFieldsParsed);
        }
        
        //Agreguem els camps multivalor si n'hi ha
        if(campMultivalorSF != null && valorsGrupContacte != null)
        {
            JsonObjectBuilder campsMultivalor = Json.createObjectBuilder();
            
            //Afegim tots els valors al objecte
            valorsGrupMC.forEach((k, v) -> {
                                                if(valorsGrupContacte.contains(k)) campsMultivalor.add(v, true);
                                                else                               campsMultivalor.add(v, false);
                                           });
            
            bodyBatch.add("interests", campsMultivalor);  
        }
                                                      
        //Agreguem la nova operació al Array d'operacions
        jsonOperacionsBatch.add(Json.createObjectBuilder().add("method", action)
                                                          .add("path", "lists/" + llistaID + "/members" + emailHash)
                                                          .add("body", bodyBatch.build().toString()));
        
        return jsonOperacionsBatch;
    }
    
    public static JsonArrayBuilder afegirCampBatch(JsonValue campFaltant, JsonArrayBuilder jsonOperationsCamps, String llistaId)
    {
        //Obtenim la Key (Nom del camp) y montem la operació del batch
        String tag = ((JsonObject)campFaltant).keySet().stream().findFirst().get();
        String name = ((JsonObject)campFaltant).getString(tag);
                
        JsonObjectBuilder bodyBatch = Json.createObjectBuilder().add("tag", tag)
                                                                .add("name", (name.length() > 50 ? name.substring(0,50) : name).toUpperCase())
                                                                .add("type", "text");
        
        //Agreguem la nova operació al Arrya d'operacions
        jsonOperationsCamps.add(Json.createObjectBuilder().add("method", "POST")
                                                          .add("path", "lists/" + llistaId + "/merge-fields")
                                                          .add("body", bodyBatch.build().toString()));
        
        return jsonOperationsCamps;
    }
    
    public static JsonArrayBuilder afegirValorGrupBatch(String valorNou, JsonArrayBuilder jsonOperationsValors, String llistaId, String grupId)
    {
        JsonObjectBuilder bodyBatch = Json.createObjectBuilder().add("name", valorNou);

        
        //Agreguem la nova operació al Arrya d'operacions
        jsonOperationsValors.add(Json.createObjectBuilder().add("method", "POST")
                                                           .add("path", "lists/" + llistaId + "/interest-categories/" + grupId + "/interests/")
                                                           .add("body", bodyBatch.build().toString()));
        
        return jsonOperationsValors;
    }
}
