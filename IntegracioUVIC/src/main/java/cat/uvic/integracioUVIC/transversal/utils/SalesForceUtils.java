package cat.uvic.integracioUVIC.transversal.utils;

import cat.uvic.integracioUVIC.integracioGrups.pojos.RestResponse;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Masu
 */
public class SalesForceUtils
{
    private static final Logger LOGGER = LogManager.getLogger(SalesForceUtils.class.getName());
    
    public static RestResponse parsejarContactes(RestResponse respostaSF, JsonArray arrayContactes, String campMail)
    {
        return parsejarContactes(respostaSF, arrayContactes, campMail, null);
    }
    
    public static RestResponse parsejarContactes(RestResponse respostaSF, JsonArray arrayContactes, String campMail, String campMultivalor)
    {
        boolean getInfoCamps = true;
        JsonObject contingut = respostaSF.getContingutResposta();
          
        Object[] camps = contingut.getJsonObject("reportMetadata").getJsonArray("detailColumns").toArray();
        Object[] files  = arrayContactes.toArray();
        
        //Obtenim les metadades dels camps
        JsonObject metadataCamps = contingut.getJsonObject("reportExtendedMetadata").getJsonObject("detailColumnInfo");
        
        JsonObjectBuilder infoCamps = Json.createObjectBuilder();
        JsonObjectBuilder infoCampMultivalor = Json.createObjectBuilder();
        JsonArrayBuilder contactes  = Json.createArrayBuilder();
           
        for(Object fila : files)
        {
            boolean mailInformat = false;
            
            JsonObjectBuilder contacte = null;

            Object[] infoFila = ((JsonObject)fila).getJsonArray("dataCells").toArray();
            
            for(int indexCamp = 0; indexCamp < camps.length;  indexCamp++)
            {
                //Obtinim el nom del camp
                String nomCamp = ((JsonString)camps[indexCamp]).getString();
                
                //Si encara no tenim l'objecte amb la info dels camps, el generem
                if(getInfoCamps)
                {
                    if(!campMultivalor.equals(nomCamp)) infoCamps.add(nomCamp, metadataCamps.getJsonObject(nomCamp).getJsonString("label"));
                    else                                infoCampMultivalor.add(nomCamp, metadataCamps.getJsonObject(nomCamp).getJsonString("label"));
                }
                
                if(nomCamp.equalsIgnoreCase(campMail))
                {                   
                    if(!((JsonObject)infoFila[indexCamp]).isNull("value"))
                    {
                        mailInformat = true;
                    }
                    else if(!getInfoCamps)
                    {
                        break;
                    }
                }

                //No craemos el objeto hasta este punto para evitar creaciones innecesarias
                if(contacte == null) contacte = Json.createObjectBuilder();
                
                contacte.add(nomCamp, ((JsonObject)infoFila[indexCamp]).getString("label"));
            }
            
            //Guardem la informació dels camps i posem la variable a false per no tornar a obtenir les mateixes dades
            if(getInfoCamps && contacte != null)
            {
                contactes.add(Json.createObjectBuilder().add("labelCamps", infoCamps));
                contactes.add(Json.createObjectBuilder().add("campMultivalor", infoCampMultivalor));
                getInfoCamps = false;
            }
            if(mailInformat)contactes.add(contacte);
        }
         
        respostaSF.setContingutResposta(Json.createObjectBuilder().add("llistaContactes", contactes).build());
        
        return respostaSF;
    }
    
    public static JsonObject getFiltreReport(String aplicacioFiltre, JsonArray condicionsFiltre, String valor, String camp, String condicio)
    {
        return getFiltreReport(aplicacioFiltre, condicionsFiltre, valor, camp, condicio, null, null);
    }
        
    public static JsonObject getFiltreReport(String aplicacioFiltre, JsonArray condicionsFiltre, String valor, String camp, String condicio, String campOrdenacio, String direccioOrdre)
    {
        JsonObjectBuilder reportMetadata = Json.createObjectBuilder();
        JsonArrayBuilder  reportFilter   = Json.createArrayBuilder();

        int numCondicions = 0;
        
        //Obtenim el filtre actual del report
        if(condicionsFiltre != null)
        {
            condicionsFiltre.forEach(t ->
                 reportFilter.add(Json.createObjectBuilder().add("value",    ((JsonObject)t).getString("value"))
                                                            .add("operator", ((JsonObject)t).getString("operator"))
                                                            .add("column",   ((JsonObject)t).getString("column")))
            );
            
            numCondicions = condicionsFiltre.size();
        }
        
        //Agreguem el nou filtre
        reportFilter.add(Json.createObjectBuilder().add("value", valor).add("operator", condicio).add("column", camp));
        reportMetadata.add("reportFilters", reportFilter.build());
        
        //Apliquem el nou filtre 
        if(!"".equals(aplicacioFiltre))
        {
            reportMetadata.add("reportBooleanFilter", aplicacioFiltre + (numCondicions > 0 ? " AND " + (numCondicions + 1) : 1));
        }
        
        //Si te ordenació l'afegim
        if(campOrdenacio != null)
        {
            JsonArrayBuilder  reportSortBy   = Json.createArrayBuilder();
            reportSortBy.add(Json.createObjectBuilder().add("sortColumn", campOrdenacio).add("sortOrder", direccioOrdre == null ? "Asc" : direccioOrdre));
            
            reportMetadata.add("sortBy", reportSortBy.build());
        }

        return Json.createObjectBuilder().add("reportMetadata", reportMetadata).build();
    }
}
