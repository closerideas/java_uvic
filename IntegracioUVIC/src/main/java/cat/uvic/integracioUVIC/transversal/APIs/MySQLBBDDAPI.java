package cat.uvic.integracioUVIC.transversal.APIs;

import cat.uvic.integracioUVIC.transversal.connectors.BBDDConnector;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Masu
 */
public class MySQLBBDDAPI
{   
    private static final String DEFAULT_ZONA_HORARIA = "serverTimezone=UTC";
    private static final String DEFAULT_MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
    
    private static final Logger LOGGER = LogManager.getLogger(MySQLBBDDAPI.class);
    
    public MySQLBBDDAPI(){}
        
    public static Connection getConnectionMySQL(String url, String usuari, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        return getConnectionMySQL(url, usuari, password, DEFAULT_MYSQL_DRIVER, null);
    }
    
    public static Connection getConnectionMySQL(String url, String usuari, String password,String driver) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        return getConnectionMySQL(url, usuari, password, driver, DEFAULT_ZONA_HORARIA);
    }
    
    public static Connection getConnectionMySQL(String url, String usuari, String password, String driver, String zonaHoraria) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        //Setejem el driver
        Class.forName(driver != null ? driver : DEFAULT_MYSQL_DRIVER);
        
        //Setejem la zona horaria
        if(zonaHoraria == null) zonaHoraria = DEFAULT_ZONA_HORARIA;
        
        //Preparem al URL de conexió
        String cadenaConexio = url.contains("?") ? url + "&" + zonaHoraria : url + "?" + zonaHoraria;
        
        LOGGER.debug("Obtenim la connexió a la BBDD: " + cadenaConexio);
        return BBDDConnector.getConnection(cadenaConexio, usuari, password, driver);
    }
}
