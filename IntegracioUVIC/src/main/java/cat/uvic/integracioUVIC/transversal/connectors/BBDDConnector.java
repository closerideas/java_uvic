package cat.uvic.integracioUVIC.transversal.connectors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Masu
 */
public class BBDDConnector
{    
    public static Connection getConnection(String url, String usuari, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        return getConnection(url, usuari, password, null);
    }
    
    public static Connection getConnection(String cadenaConexio, String usuari, String password, String driver) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {        
        return DriverManager.getConnection(cadenaConexio, usuari, password);
    }
}
