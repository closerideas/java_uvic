package cat.uvic.integracioUVIC.inicialitzacio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.logging.log4j.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Masu
 */
public class inicialitzacioContextListener implements ServletContextListener
{

    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        System.setProperty("log4j.configurationFile", "/uvic/integraciogrups/log4j2.xml");
        
        InputStream input;
        try
        {
            input = new FileInputStream("/uvic/integraciogrups/Connexions.properties");
            
            Properties prop = new Properties();
            prop.load(input);

            System.setProperty("urlMySQL", prop.getProperty("urlMySQL"));
            System.setProperty("usuariMySQL", prop.getProperty("usuariMySQL"));
            System.setProperty("passwordMySQL", prop.getProperty("passwordMySQL"));       
        } 
        catch (FileNotFoundException ex) 
        {
            Logger logger = LogManager.getLogger(inicialitzacioContextListener.class.getName());
            logger.fatal("No s'ha trobat l'arxiu per carregar la configuració de la base de dades:\n" + ex.getMessage());
        }
        catch (IOException ex)
        {
            Logger logger = LogManager.getLogger(inicialitzacioContextListener.class.getName());
            logger.fatal("S'ha generat un error a l'hora de llegir l'arxiu de configuració de la base de dades:\n" + ex.getMessage());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
        
    } 
}
